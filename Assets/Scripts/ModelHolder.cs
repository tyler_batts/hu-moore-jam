﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelHolder : MonoBehaviour {
    public List<Mesh> asteroidsSmall;
    public List<Mesh> asteroidsMedium;
    public List<Mesh> asteroidsBig;

    public Mesh playerOneShip, playerTwoShip;

    public static ModelHolder modelHolder;

    void Awake () {
        modelHolder = this;
    }
}
