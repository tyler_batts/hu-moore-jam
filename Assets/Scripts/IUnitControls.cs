﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnitControls {

    void Shoot ();

    void TakeDamage (float damageTaken);

    void Die ();
}
