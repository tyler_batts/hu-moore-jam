﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour {

    public enum PoolInitializeTime { Start, Awake, Manual }
    [SerializeField] PoolInitializeTime initializeTime;

    [SerializeField] GameObject pooledObjectPrefab;
    [SerializeField] int sizeOfPool;
    [SerializeField] bool canExpand;
    [SerializeField] bool startEnabled;

    List<GameObject> pool;

    void Start () {
        pool = new List<GameObject> ();
        if (initializeTime == PoolInitializeTime.Start) {
            InitializePool ();
        }
    }

    void Awake () {
        if (initializeTime == PoolInitializeTime.Awake) {
            InitializePool ();
        }
    }

    public void InitializePool () {
        for (int i = 0; i < sizeOfPool; i++) {
            GameObject obj = (GameObject) Instantiate (pooledObjectPrefab);
            obj.transform.SetParent(transform);
            obj.transform.position = Vector3.zero;
            obj.SetActive (startEnabled);
            pool.Add (obj);
        }
    }

    public void ClearPool () {
        foreach (GameObject obj in pool) {
            Destroy (obj);
        }
        pool.Clear ();
    }

    public GameObject GetDisabledPooledObject () {
        foreach (GameObject obj in pool) {
            if (!obj.activeInHierarchy) {
                return obj;
            }
        }

        if (canExpand) {
            return AddObjectToPool ();
        }

        return null;
    }

    public GameObject AddObjectToPool () {
        if (canExpand) {
            GameObject obj = (GameObject) Instantiate (pooledObjectPrefab);
            obj.transform.SetParent (transform);
            obj.SetActive (startEnabled);
            pool.Add (obj);
            return obj;
        }
        return null;
    }

    public List<GameObject> GetPooledObjects {
        get {
            return pool;
        }
    }

    public GameObject PooledObjectPrefab {
        get {
            return pooledObjectPrefab;
        }

        set {
            pooledObjectPrefab = value;
        }
    }
}