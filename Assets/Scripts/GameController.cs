﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public ObjectPool asteroidPool;
    public Text score, highscore;

    private void Start () {
        Constants.OnGameLoad ();
        if (asteroidPool == null) {
            asteroidPool = GetComponent<ObjectPool> ();
        }

        Vector3 playerPos = GameObject.FindGameObjectWithTag ("Player").transform.position;
        foreach (GameObject obj in asteroidPool.GetPooledObjects) {
            obj.transform.position = playerPos + new Vector3 (Random.Range (-50, 50), 0f, Random.Range (-50, 50));
            obj.GetComponent<AsteroidController> ().ResetAsteroid (false);
            obj.GetComponent<AsteroidController> ().SetAsteroidStartState = AsteroidController.AsteroidStartState.Big;
        }
    }

    private void Update () {
        score.text = "Score: " + Mathf.Floor(Constants.score);
        highscore.text = "High Score: " + PlayerPrefs.GetFloat ("High Score");
    }

    private void FixedUpdate () {
        Constants.score += 0.5f;
    }

    private void OnApplicationQuit () {
        Constants.OnQuit ();
    }
}
