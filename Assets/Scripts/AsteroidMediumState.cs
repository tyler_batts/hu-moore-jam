﻿using UnityEngine;

public class AsteroidMediumState : IAsteroidState {
    private readonly AsteroidController asteroid;

    public AsteroidMediumState (AsteroidController controller) {
        asteroid = controller;
    }

    public void Die () {
        asteroid.DestroyThis ();
    }

    public void Split () {
        asteroid.SplitToNewAsteroids (AsteroidController.AsteroidStartState.Small);
        ToSmallState ();
    }

    public void ToMediumState () { }

    public void ToSmallState () {
        asteroid.currentState = asteroid.asteroidSmallState;
    }

    public void UpdateState () {
        if (asteroid.CurrentHealth <= 0) {
            Split ();
            ToSmallState ();
            Die ();
        }
    }

    public void OnCollisionEnter (Collision other) {
        if (other.gameObject.CompareTag ("Projectile")) {
            ProjectileScript ps = other.gameObject.GetComponent<ProjectileScript> ();
            asteroid.CurrentHealth -= ps.DamageAmount;
            ps.DestroyThis ();
        }
    }
}
