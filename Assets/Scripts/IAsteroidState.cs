﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAsteroidState {

    void ToSmallState ();

    void ToMediumState ();

    void Die ();

    void Split ();

    void UpdateState ();

    void OnCollisionEnter (Collision other);
}
