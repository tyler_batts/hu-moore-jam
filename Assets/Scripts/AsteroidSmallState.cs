﻿using UnityEngine;

public class AsteroidSmallState : IAsteroidState {
    private readonly AsteroidController asteroid;

    public AsteroidSmallState (AsteroidController controller) {
        asteroid = controller;
    }

    public void Die () {
        asteroid.DestroyThis ();
    }

    public void Split () { }

    public void ToMediumState () { }

    public void ToSmallState () { }

    public void UpdateState () {
        if (asteroid.CurrentHealth <= 0) {
            Die ();
        }
    }

    public void OnCollisionEnter (Collision other) {
        if (other.gameObject.CompareTag ("Projectile")) {
            ProjectileScript ps = other.gameObject.GetComponent<ProjectileScript> ();
            asteroid.CurrentHealth -= ps.DamageAmount;
            ps.DestroyThis ();
        }
    }
}

