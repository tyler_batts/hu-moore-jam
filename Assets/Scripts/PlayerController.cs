using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour, IUnitControls {

    public enum WeaponType { None, Chainstriker, Fighter, MineDropper, MIRV, Scatter }
    public WeaponType type;

    [SerializeField] private float _startingLives = 3;
	[SerializeField] private float _baseMoveSpeed, _maxSpeed, _baseTurningSpeed;

    [SerializeField] private GameObject baseProjectilePrefab, mineProjectilePrefab;

	private Rigidbody rb;

    private float _livesRemaining;
    private bool _isDashing, _isInvulnerable, _isDead;

    private ObjectPool pool;
    [SerializeField] GameObject muzzle;
    [SerializeField] ParticleSystem particleSystemBack, particleSystemDash, particleSystemLeft, particleSystemRight;

    private bool canShoot = true;

    void Start () {
		rb = GetComponent<Rigidbody>();
        pool = GetComponent<ObjectPool> ();

        _livesRemaining = StartingLives;
	}

    private void Update () {
        Vector3 pos = transform.position;
        pos.y = 0;
        transform.position = pos;

        if (Input.GetButton("Fire") && canShoot) {
            Shoot ();
        }

        if (LivesRemaining <= 0) {
            Die ();
        }
    }

    void FixedUpdate () {
        ParticleSystem.EmissionModule emission = particleSystemBack.emission;
        ParticleSystem.EmissionModule emL = particleSystemLeft.emission;
        ParticleSystem.EmissionModule emR = particleSystemRight.emission;
        if (!_isDead) {
            if (!_isDashing) {
                emission.enabled = false;
                if (Input.GetAxis ("Vertical") > 0f) {
                    Vector3 targetPos = (transform.forward * BaseMoveSpeed);
                    rb.AddForce (targetPos, ForceMode.Acceleration);
                    rb.velocity = Vector3.ClampMagnitude (rb.velocity, MaxSpeed);
                    emission.enabled = true;
                } else if (Input.GetAxis ("Vertical") < 0) {
                    StartCoroutine (Dash ());
                } else if (Input.GetAxis ("Vertical") == 0f && rb.velocity.magnitude > 0.2f) {
                    rb.velocity -= rb.velocity * 0.05f;
                } else {
                    rb.velocity = Vector3.zero;
                }
            }

            if (Mathf.Abs (Input.GetAxis ("Horizontal")) > 0) {
                rb.MoveRotation (rb.rotation * Quaternion.Euler (transform.rotation.x, transform.rotation.y + (Input.GetAxis ("Horizontal") * BaseTurningSpeed), transform.rotation.z));

                if (Input.GetAxis ("Horizontal") > 0) {
                    emR.enabled = true;
                    emL.enabled = false;
                } else if (Input.GetAxis ("Horizontal") < 0) {
                    emR.enabled = false;
                    emL.enabled = true;
                }
            } else {
                emR.enabled = false;
                emL.enabled = false;
            }
        } else {
            emission.enabled = false;
            emL.enabled = false;
            emR.enabled = false;

            rb.velocity = Vector3.zero;
        }
	}

    private IEnumerator Dash () {
        ParticleSystem.EmissionModule emission = particleSystemDash.emission;
        emission.enabled = true;
        _isDashing = true;
        rb.AddForce (transform.forward * BaseMoveSpeed * 2f, ForceMode.VelocityChange);
        yield return new WaitForSeconds(0.2f);
        _isDashing = false;
        emission.enabled = false;
    }

    public void Shoot () {
        canShoot = false;
        switch (type) {
            default:
            GameObject proj = pool.GetDisabledPooledObject ();
            ProjectileScript ps = proj.GetComponent<ProjectileScript> ();
            ps.SetProjectileType (ProjectileScript.ProjectileType.Basic);
            proj.transform.position = muzzle.transform.position;
            proj.GetComponent<Rigidbody> ().velocity = transform.forward * (ps.ShootSpeed + rb.velocity.magnitude);
            proj.layer = LayerMask.NameToLayer ("Player");
            ps.shooter = gameObject;
            proj.SetActive (true);
            StartCoroutine (ShotTimer ());
            break;

            case WeaponType.Fighter:
            break;

            case WeaponType.MineDropper:
            pool.ClearPool ();
            pool.PooledObjectPrefab = mineProjectilePrefab;
            pool.InitializePool ();
            proj = pool.GetDisabledPooledObject ();
            ps = proj.GetComponent<ProjectileScript> ();
            ps.SetProjectileType (ProjectileScript.ProjectileType.Basic);
            proj.transform.position = muzzle.transform.position;
            proj.GetComponent<Rigidbody> ().velocity = transform.forward * (ps.ShootSpeed + rb.velocity.magnitude);
            proj.layer = LayerMask.NameToLayer ("Player");
            ps.shooter = gameObject;
            proj.SetActive (true);
            StartCoroutine (ShotTimer ());
            break;

            case WeaponType.MIRV:
            break;

            case WeaponType.Scatter:
            proj = pool.GetDisabledPooledObject ();
            ps = proj.GetComponent<ProjectileScript> ();
            ps.SetProjectileType (ProjectileScript.ProjectileType.Basic);
            proj.transform.position = muzzle.transform.position;
            proj.GetComponent<Rigidbody> ().velocity = Quaternion.Euler (0f, 5f, 0f) * transform.forward * (ps.ShootSpeed + rb.velocity.magnitude);
            proj.layer = LayerMask.NameToLayer ("Player");
            ps.shooter = gameObject;
            proj.transform.LookAt (proj.transform.position + proj.GetComponent<Rigidbody> ().velocity);
            proj.SetActive (true);

            proj = pool.GetDisabledPooledObject ();
            ps = proj.GetComponent<ProjectileScript> ();
            ps.SetProjectileType (ProjectileScript.ProjectileType.Basic);
            proj.transform.position = muzzle.transform.position;
            proj.GetComponent<Rigidbody> ().velocity = transform.forward * (ps.ShootSpeed + rb.velocity.magnitude);
            proj.layer = LayerMask.NameToLayer ("Player");
            ps.shooter = gameObject;
            proj.transform.LookAt (proj.transform.position + proj.GetComponent<Rigidbody> ().velocity);
            proj.SetActive (true);

            proj = pool.GetDisabledPooledObject ();
            ps = proj.GetComponent<ProjectileScript> ();
            ps.SetProjectileType (ProjectileScript.ProjectileType.Basic);
            proj.transform.position = muzzle.transform.position;
            proj.GetComponent<Rigidbody> ().velocity = Quaternion.Euler (0f, -5f, 0f) * transform.forward * (ps.ShootSpeed + rb.velocity.magnitude);
            proj.layer = LayerMask.NameToLayer ("Player");
            ps.shooter = gameObject;
            proj.transform.LookAt (proj.transform.position + proj.GetComponent<Rigidbody> ().velocity);
            proj.SetActive (true);
            StartCoroutine (ShotTimer ());
            break;
        }
    }

    IEnumerator ShotTimer () {
        yield return new WaitForSeconds (0.1f);
        canShoot = true;
    }

    public void TakeDamage (float damageTaken) {
        if (!_isInvulnerable)
            _livesRemaining -= damageTaken;

        StartCoroutine (Invulnerable (3f));
    }

    IEnumerator Invulnerable (float lengthOfInvulnerability) {
        _isInvulnerable = true;
        Color tempColor = GetComponentInChildren<MeshRenderer> ().material.color;
        tempColor.a = 0.5f;
        GetComponentInChildren<MeshRenderer> ().material.color = tempColor;
        yield return new WaitForSeconds (lengthOfInvulnerability);
        _isInvulnerable = false;
        tempColor.a = 1f;
        GetComponentInChildren<MeshRenderer> ().material.color = tempColor;
    }

    public void Die () {
        Debug.Log ("Player died");
        _isDead = true;
    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.CompareTag("Asteroid") && !_isInvulnerable) {
            TakeDamage (1f);
            AsteroidController ac = collision.gameObject.GetComponent<AsteroidController> ();
            switch (ac.SetAsteroidStartState) {
                case AsteroidController.AsteroidStartState.Big:
                ac.SplitToNewAsteroids(AsteroidController.AsteroidStartState.Medium);
                break;

                case AsteroidController.AsteroidStartState.Medium:
                ac.SplitToNewAsteroids(AsteroidController.AsteroidStartState.Small);
                break;

                case AsteroidController.AsteroidStartState.Small:
                ac.DestroyThis ();
                break;
            }
        }
    }

    #region Properties
    public float StartingLives {
        get {
            return _startingLives;
        }
    }

    public float LivesRemaining {
        get {
            return _livesRemaining;
        }

        set {
            _livesRemaining = value;
        }
    }

    public float BaseMoveSpeed {
        get {
            return _baseMoveSpeed;
        }
    }

    public float MaxSpeed {
        get {
            return _maxSpeed;
        }
    }

    public float BaseTurningSpeed {
        get {
            return _baseTurningSpeed;
        }
    }

    public bool IsInvulnerable {
        get {
            return _isInvulnerable;
        }
    }
    #endregion
}