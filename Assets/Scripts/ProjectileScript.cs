﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour {

	public enum ProjectileType { None, Basic, ChainLightning, Mine }
    private ProjectileType projectileType;

    [SerializeField] private float shootSpeed;
    [SerializeField]
    private float damage;

    public GameObject shooter;
    private float distance;

    private void Start () {

    }

    private void Update () {
        distance = Vector3.Distance (transform.position, shooter.transform.position);
        if (projectileType == ProjectileType.Basic) {
            if (distance > 50f) {
                DestroyThis ();
            }
        }
    }

    public void SetProjectileType (ProjectileType type) {
        projectileType = type;
    }

    public void DestroyThis () {
        gameObject.SetActive (false);
    }

    #region Properties
    public float ShootSpeed {
        get {
            return shootSpeed;
        }
    }

    public float DamageAmount { 
        get {
            return damage;
        }
    }
    #endregion
}