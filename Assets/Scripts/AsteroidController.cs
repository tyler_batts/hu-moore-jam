﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {
    public enum AsteroidStartState { Small = 1, Medium = 2, Big = 3 }
    [SerializeField] AsteroidStartState asteroidStartState;
    [SerializeField] private float baseHealth, baseMovementSpeed;
    private float currentHealth;

    [HideInInspector] public IAsteroidState currentState;
    [HideInInspector] public AsteroidBigState asteroidBigState;
    [HideInInspector] public AsteroidMediumState asteroidMediumState;
    [HideInInspector] public AsteroidSmallState asteroidSmallState;

    private Rigidbody rb;
    [SerializeField] private MeshRenderer mr;
    [SerializeField] private MeshFilter mf;
    [SerializeField] private MeshCollider mc;

    public GameObject asteroidPrefab;
    public GameObject oneUpPrefab;

    void Awake () {
        if (asteroidBigState == null)
            asteroidBigState = new AsteroidBigState (this);
        if (asteroidMediumState == null)
            asteroidMediumState = new AsteroidMediumState (this);
        if (asteroidSmallState == null)
            asteroidSmallState = new AsteroidSmallState (this);
    }

    void Update () {
        currentState.UpdateState ();

        switch (asteroidStartState) {
            case AsteroidStartState.Small:
            gameObject.name = "Small Asteroid";
            break;

            case AsteroidStartState.Medium:
            gameObject.name = "Medium Asteroid";
            break;

            case AsteroidStartState.Big:
            gameObject.name = "Big Asteroid";
            break;
        }
    }

    void OnCollisionEnter (Collision collision) {
        currentState.OnCollisionEnter (collision);
    }

    public void ResetAsteroid (bool randomize) {
        if (randomize) {
            System.Array values = System.Enum.GetValues (typeof (AsteroidStartState));
            System.Random random = new System.Random (Random.Range(0, 95565));
            asteroidStartState = (AsteroidStartState) values.GetValue (random.Next (values.Length));
        }

        rb = GetComponent<Rigidbody> ();
        Mesh randomMesh = null;
        switch (asteroidStartState) {
            case AsteroidStartState.Big:
            if (ModelHolder.modelHolder.asteroidsBig != null)
                randomMesh = ModelHolder.modelHolder.asteroidsBig [Random.Range (0, ModelHolder.modelHolder.asteroidsBig.Count - 1)];
            currentState = asteroidBigState;
            baseHealth = 5;
            rb.mass = 5f;
            gameObject.name = "Big Asteroid";
            break;

            case AsteroidStartState.Medium:
            if (ModelHolder.modelHolder.asteroidsMedium != null)
                randomMesh = ModelHolder.modelHolder.asteroidsMedium [Random.Range (0, ModelHolder.modelHolder.asteroidsMedium.Count - 1)];
            currentState = asteroidMediumState;
            rb.mass = 3f;
            baseHealth = 3;
            gameObject.name = "Medium Asteroid";
            break;

            case AsteroidStartState.Small:
            if (ModelHolder.modelHolder.asteroidsSmall != null)
                randomMesh = ModelHolder.modelHolder.asteroidsSmall [Random.Range (0, ModelHolder.modelHolder.asteroidsSmall.Count - 1)];
            currentState = asteroidSmallState;
            baseHealth = 1;
            rb.mass = 1f;
            gameObject.name = "Small Asteroid";
            break;
        }
        currentHealth = baseHealth;
        if (randomMesh != null) {
            mf.mesh = randomMesh;
            mc.sharedMesh = randomMesh;
        }

        Vector2 startDir = Random.insideUnitCircle;
        rb.velocity = new Vector3 (startDir.x, 0f, startDir.y) * Random.Range (0.1f, baseMovementSpeed);
        transform.LookAt (Random.insideUnitSphere * 10f);
    }

    public void SplitToNewAsteroids (AsteroidStartState stateToGoTo) {
        for (int i = 0; i < 3; i++) {
            GameObject newAsteroid = FindObjectOfType<GameController> ().asteroidPool.GetDisabledPooledObject ();
            newAsteroid.transform.position = transform.position;
            AsteroidController ac = newAsteroid.GetComponent<AsteroidController> ();
            ac.asteroidStartState = stateToGoTo;
            ac.ResetAsteroid (false);
        }
        DestroyThis ();
    }

    public void DestroyThis () {
        // Score TODO:
        switch (asteroidStartState) {
            case AsteroidStartState.Small:
            Constants.score += 100;
            float r = Random.Range (0f, 1f);
            if (r > 0.75) {
                Instantiate (oneUpPrefab);
            }
            break;

            case AsteroidStartState.Medium:
            Constants.score += 150;
            break;

            case AsteroidStartState.Big:
            Constants.score += 200;
            break;
        }

        
        Constants.score += 1f;
        gameObject.SetActive (false);
    }

    #region Properties
    public float CurrentHealth {
        get {
            return currentHealth;
        }

        set {
            currentHealth = value;
        }
    }

    public AsteroidStartState SetAsteroidStartState {
        get {
            return asteroidStartState;
        }

        set {
            asteroidStartState = value;
        }
    }
    #endregion
}
