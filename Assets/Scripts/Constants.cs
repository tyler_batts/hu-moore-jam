﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants {
    public static float score;
    private static float highScore;

    public static void OnGameLoad () {
        highScore = PlayerPrefs.GetFloat ("High Score");
    }

    public static void Save () {
        if (score > highScore)
            PlayerPrefs.SetFloat ("High Score", score);
    }

    public static void OnQuit () {
        Save ();
    }
}
