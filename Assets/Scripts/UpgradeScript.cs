﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeScript : MonoBehaviour {
    public enum UpgradeType { None, Chainstriker, Fighter, MineDropper, MIRV, Scatter, ExtraLife }
    public UpgradeType type;

    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.CompareTag("Player")) {
            PlayerController pc = collision.gameObject.GetComponent<PlayerController> ();
            switch (type) {
                case UpgradeType.Chainstriker:
                pc.type = PlayerController.WeaponType.Chainstriker;
                Destroy (gameObject);
                break;

                case UpgradeType.Fighter:
                pc.type = PlayerController.WeaponType.Fighter;
                Destroy (gameObject);
                break;

                case UpgradeType.MineDropper:
                pc.type = PlayerController.WeaponType.MineDropper;
                Destroy (gameObject);
                break;

                case UpgradeType.MIRV:
                pc.type = PlayerController.WeaponType.MIRV;
                Destroy (gameObject);
                break;

                case UpgradeType.Scatter:
                pc.type = PlayerController.WeaponType.Scatter;
                Destroy (gameObject);
                break;

                case UpgradeType.ExtraLife:
                pc.LivesRemaining++;
                Destroy (gameObject);
                break;
            }
        }
    }
}
