﻿using UnityEngine;

public class AsteroidBigState : IAsteroidState {
    private readonly AsteroidController asteroid;

    public AsteroidBigState (AsteroidController controller) {
        asteroid = controller;
    }

    public void Die () {
        asteroid.DestroyThis ();
    }

    public void Split () {
        asteroid.SplitToNewAsteroids (AsteroidController.AsteroidStartState.Medium);
        ToMediumState ();
    }

    public void ToMediumState () {
        asteroid.currentState = asteroid.asteroidMediumState;
    }

    public void ToSmallState () {
        asteroid.currentState = asteroid.asteroidSmallState;
    }

    public void UpdateState () {
        if (asteroid.CurrentHealth <= 0) {
            Split ();
            ToMediumState ();
            Die ();
        }
    }

    public void OnCollisionEnter (Collision other) {
        if (other.gameObject.CompareTag("Projectile")) {
            ProjectileScript ps = other.gameObject.GetComponent<ProjectileScript> ();
            asteroid.CurrentHealth -= ps.DamageAmount;
            ps.DestroyThis ();
        }
    }
}

